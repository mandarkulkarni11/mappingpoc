public with sharing class JSONConst {
   
   public static string samplePayload1 = '{'+ 
                                ' "id": 865605, '+ 
                                ' "name": "FWMT-424 : campaign mapping test", '+
                                ' "campaignLead": null, ' +
                                ' "objective": "test",'+
                                ' "budget": null,'+
                                ' "startDate": null,'+
                                ' "endDate": null,'+
                                ' "createDate": "2021-10-12T16:16:18.000Z",'+
                                ' "lastUpdate": "2022-03-01T17:21:35.000Z",'+
                                ' "createdBy": "e2euser1@example.com",'+
                                ' "lastUpdatedBy": "arao@mercuryhealthcare.com",'+
                                ' "campaignType": "acquisition",'+
                                ' "executionStatus": "draft", '+
                                ' "tactics" : [{ ' +
                                    ' "id": 2322,'+
                                    ' "name": "My tac",'+
                                    ' "description": null,'+
                                    ' "callToActionWebPage": "blah.com",'+
                                    ' "callToActionPhoneNumber": null'+
                                    ' }]' + 
                                '}';

   public static string samplePayload2 = '{'+ 
                                    ' "id": 865605, '+ 
                                    ' "name": "FWMT-424 : campaign mapping test", '+
                                    ' "campaignLead": null, ' +
                                    ' "objective": "test",'+
                                    ' "budget": null,'+
                                    ' "startDate": null,'+
                                    ' "endDate": null,'+
                                    ' "createDate": "2021-10-12T16:16:18.000Z",'+
                                    ' "lastUpdate": "2022-03-01T17:21:35.000Z",'+
                                    ' "createdBy": "e2euser1@example.com",'+
                                    ' "lastUpdatedBy": "arao@mercuryhealthcare.com",'+
                                    ' "campaignType": "acquisition",'+
                                    ' "executionStatus": "draft",'+
                                    ' "communicationStartDate": null,'+
                                    ' "communicationEndDate": null,'+
                                    ' "trackResponsesMonths": 12,'+
                                    ' "includeResponsesFrom": "any_household_member",'+
                                    ' "facilities": [],'+
                                    ' "visitTypes": [],'+
                                    ' "serviceCategories": [],'+
                                    ' "subServiceCategories": [],'+
                                    ' "customServiceCategories": [],'+
                                    ' "customSubServiceCategories": [],'+
                                    ' "procedures": [],'+
                                    ' "diagnoses": [],'+
                                    ' "msdrgs": [],'+
                                    ' "costSchedules": [],'+
                                    ' "tactics": ['+
                                    '    {'+
                                    '    "id": 2322,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {' +
                                    '    "id": 2323,'+
                                    '    "name": "My Tac2",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' +
                                    '    {'+
                                    '    "id": 2324,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2325,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2326,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2327,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2328,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2329,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null,'+
                                    '    "callToAction" : ["Announcement", "Appointment request", "Brand awareness"] '+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2330,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2331,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2332,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2333,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2334,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2335,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2336,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2337,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2338,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2339,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2340,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2341,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2342,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2343,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2344,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2345,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2346,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2347,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2348,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2349,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2350,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2351,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2352,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2353,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2354,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2355,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2356,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2357,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2358,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2359,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    },' + 
                                    '    {'+
                                    '    "id": 2360,'+
                                    '    "name": "My Tac1",'+
                                    '    "description": "Some Description",'+
                                    '    "callToActionWebPage": "blah.com",'+
                                    '    "callToActionPhoneNumber": null'+
                                    '    }' + 
                                    ']'+
                                '}';

    public static string samplePayload3 = '{' +
                        ' "id": 865605, '+
                        ' "name": "FWMT-424 : campaign mapping test",'+
                        ' "campaignLead": null,'+
                        ' "objective": "test",'+
                        ' "budget": null,'+
                        ' "startDate": null,'+
                        ' "endDate": null,'+
                        ' "createDate": "2021-10-12T16:16:18.000Z",'+
                        ' "lastUpdate": "2022-03-01T17:21:35.000Z",'+
                        ' "createdBy": "e2euser1@example.com",'+
                        ' "lastUpdatedBy": "arao@mercuryhealthcare.com",'+
                        ' "campaignType": "acquisition",'+
                        ' "executionStatus": "draft",'+
                        ' "communicationStartDate": null,'+
                        ' "communicationEndDate": null,'+
                        ' "trackResponsesMonths": 12,'+
                        ' "includeResponsesFrom": "any_household_member",'+
                        ' "facilities": [],'+
                        ' "visitTypes": [],'+
                        ' "serviceCategories": [],'+
                        ' "subServiceCategories": [],'+
                        ' "customServiceCategories": [],'+
                        ' "customSubServiceCategories": [],'+
                        ' "procedures": [],'+
                        ' "diagnoses": [],'+
                        ' "msdrgs": [],'+
                        ' "costSchedules": [],'+
                        ' "tactics": ['+
                        ' {'+
                        '    "id": 2322,'+
                        '    "name": "My tac",'+
                        '    "description": null,'+
                        '    "callToActionWebPage": "blah.com",'+
                        '    "callToActionPhoneNumber": null,'+
                        '    "trackingSets": ['+
                        '    {'+
                        '        "id": 2765,'+
                        '        "sourceProvider": "healthgrades",'+
                        '        "sourceProviderExternalId": null,'+
                        '        "campaignChannel": "direct_mail",'+
                        '        "agency": "client",'+
                        '        "facility": null,'+
                        '        "serviceCategory": null,'+
                        '        "webPages": [],'+
                        '        "phoneNumbers": [],'+
                        '        "webPageParameters": ['+
                        '        {'+
                        '            "id": 14980,'+
                        '            "key": "hgcrm_channel",'+
                        '            "value": "direct_mail"'+
                        '        },'+
                        '        {'+
                        '            "id": 14981,'+
                        '            "key": "hgcrm_source",'+
                        '            "value": "healthgrades"'+
                        '        },'+
                        '        {'+
                        '            "id": 14982,'+
                        '            "key": "hgcrm_agency",'+
                        '            "value": "client"'+
                        '        }'+
                        '        ]'+
                        '    }'+
                        '    ]'+
                        '}'+
                        ']'+
                    '}';
    
        
}


