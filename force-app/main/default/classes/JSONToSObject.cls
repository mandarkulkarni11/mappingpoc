/*
    JSON Parsing - 
    
    Approach 1: Brute Force (greedy) 
    Iterates each json node and finds corresponding mapping attribute
    Assumptions and Drawbacks:
    1. Depends on External Ids to relate records using single SObject list
    2. The Parent Identifier must be present in the JSON and must come first before any child records, so the relationship between parent and child can be setup   
*/
public with sharing class JSONToSObject {
    
    public List<SObject> lstResult;
    public JSONToSObject() {
        this.lstResult = new List<SObject>();
    }
    
    //Stores Attribute Mappings<AttributeName,AttributeMapping__mdt> by ObjectMapping (Uses ObjectMapping__r.DeveloperName as key)
    private static final Map<String, Map<String, AttributeMapping__mdt>> fieldMappingsByObjectMap = new Map<String, Map<String, AttributeMapping__mdt>>();
    
    //Stores object mappings by developer name
    private static final Map<String, ObjectMapping__mdt> objectMapsByDeveloperName;
    
    //Populate above mentioned maps  
    static {
        objectMapsByDeveloperName = ObjectMapping__mdt.getAll();
        if(!objectMapsByDeveloperName.isEmpty()){
            
            Map<String, AttributeMapping__mdt> fieldMapsByDevName = AttributeMapping__mdt.getAll();
            if(!fieldMapsByDevName.isEmpty()){
                
                for(AttributeMapping__mdt recFieldMap : fieldMapsByDevName.values()){
                    if(!fieldMappingsByObjectMap.containsKey(recFieldMap.ObjectMapping__r.DeveloperName)){
                        fieldMappingsByObjectMap.put(recFieldMap.ObjectMapping__r.DeveloperName, new Map<String, AttributeMapping__mdt>());
                    }
                    fieldMappingsByObjectMap.get(recFieldMap.ObjectMapping__r.DeveloperName).put(recFieldMap.Attribute__c, recFieldMap);
                }
            }
        }
    }
    
    private void parseJsonNode(JSONParser Parser, string mappingName, Relationship relationship){
        if(parser.getCurrentToken() == JSONTOKEN.START_OBJECT){
            System.debug('NODE PARSING::CPU TIME::' + Limits.getCpuTime());
            System.debug('NODE PARSING::HEAP SIZE::' + Limits.getHeapSize());

            string recordExternalId = '';
            string recrodExternalIdFieldApi = '';

            //Create record
            SObject record = this.instantiateRecord(mappingName, relationship);
            this.lstResult.add(record);
            while(parser.nextToken() != JSONTOKEN.END_OBJECT){
                if(parser.getCurrentToken() == JSONTOKEN.FIELD_NAME){
                    string currentAttributeName = parser.getCurrentName();
                    
                    AttributeMapping__mdt attributeInfo = fieldMappingsByObjectMap.get(mappingName).get(currentAttributeName);
                    if(attributeInfo == null) continue; //If no mapping skip over

                    System.debug('ATTRIBUTE::' + currentAttributeName);
                    System.debug('ATTRIBUTE TYPE::' + attributeInfo.FieldType__c);
                    switch on attributeInfo.FieldType__c{
                        when 'ChildRelationship'{//Means mapping to another object
                            if(parser.nextToken() == JSONTOKEN.START_ARRAY){ //If its an array of objects
                                Relationship relationshipInfo = new Relationship(recrodExternalIdFieldApi, recordExternalId, record.getSObjectType(), attributeInfo.RelationshipField__c);
                                while(parser.nextToken() != JSONTOKEN.END_ARRAY){
                                    this.parseJsonNode(parser, attributeInfo.ReferenceMapping__r.DeveloperName, relationshipInfo);
                                }
                            }else if(parser.nextToken() == JSONTOKEN.START_OBJECT){//If its single object
                                Relationship relationshipInfo = new Relationship(recrodExternalIdFieldApi, recordExternalId, record.getSObjectType(), attributeInfo.RelationshipField__c);
                                this.parseJsonNode(parser, attributeInfo.ReferenceMapping__r.DeveloperName, relationshipInfo);
                            }
                        }
                        when 'MultiPicklist'{ //Means mapping to multiselect picklist, so will be expecting JSON array eg. { "attr" : ["A", "B", "C"]}
                            List<String> lstVals = new List<String>();
                            while(parser.nextToken() != JSONTOKEN.END_ARRAY){
                                if(parser.getCurrentToken() == JSONTOKEN.VALUE_STRING){
                                    lstVals.add(parser.getText());
                                }
                            }
                            record.put(attributeInfo.FieldAPI__c, String.join(lstVals, ';'));
                        }
                        when else{ //Means mapping to string or picklist field
                            parser.nextToken();
                            record.put(attributeInfo.FieldAPI__c, parser.getText());
                            if(attributeInfo.IsExternalId__c){
                                recordExternalId = parser.getText();
                                recrodExternalIdFieldApi = attributeInfo.FieldAPI__c;
                            }
                        }
                    }
                } 
            }
        }
    }
    
    private SObject instantiateRecord(string mappingName, Relationship relationship){
        SObject record = (SObject)Type.forName(objectMapsByDeveloperName.get(mappingName).ObjectAPI__c).newInstance();
        
        //Means its a child record and connection to parent needs to be setup.
        if(relationship != null){
            SObject parentRef = relationship.parentObjType.newSObject();
            system.debug('-RELATIONSHIP FIELD->>' + relationship.externalIdFieldApi);
            parentRef.put(relationship.externalIdFieldApi, relationship.parentExternalId);
            record.putSObject(relationship.lookupRelationshipField, parentRef);
        }
        return record;
    }

    public List<SObject> parse(string payload, String mappingName){
        JSONParser parser = JSON.createParser(payload);
        while(parser.nextToken() != null){
            this.parseJsonNode(parser, mappingName, null);
        }
        return this.lstResult;
    }
    
    public class Relationship{
        public string parentExternalId;
        public Schema.SObjectType parentObjType;
        public string externalIdFieldApi;
        public string lookupRelationshipField;
        public Relationship(){}
        public Relationship(string recrodExternalIdFieldApi, string recordExternalId, Schema.SObjectType parentObjType, string lookupRelationshipField){
            this.parentExternalId = recordExternalId; //Parent External Id
            this.externalIdFieldApi = recrodExternalIdFieldApi; //Parent External Id Field Api
            this.parentObjType = parentObjType; //Reference to Parent Object
            
            if(String.isNotEmpty(lookupRelationshipField) && 
                lookupRelationshipField.endsWith('__c')){
                this.lookupRelationshipField = lookupRelationshipField.replace('__c', '__r'); //Lookup relationship field
            }
        }
    }

    public class ParsingException extends Exception{}
}
